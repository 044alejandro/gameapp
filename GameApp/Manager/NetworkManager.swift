

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    
    func loadResult(isWinner: Bool, completionHandler: @escaping(String?, String?) -> Void) {
        let urlComponents = URLComponents(string: "https://2llctw8ia5.execute-api.us-west-1.amazonaws.com/prod")!
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request){ data, response, error in
            
            if let error = error {
                completionHandler(nil, error.localizedDescription)
            }
            
            if let data = data {
                do{
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(Results.self, from: data)
                        switch isWinner{
                        case true:
                            completionHandler(response.winner, nil)
                        case false:
                            completionHandler(response.loser, nil)
                        }
                } catch let error as NSError {
                    completionHandler(nil, error.localizedDescription)
                }
             }
        }.resume()
    }
}
