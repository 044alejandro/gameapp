
import UIKit

class StartGameViewController: UIViewController {

    @IBOutlet weak var startButton: UIButton!
    var buttonImage: UIButton?
    var counter: Int = 0
    var start = NSDate()
    var end = NSDate()
    var result = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startButton.alpha = 1
        counter = 0
        result = 0
    }
    
    func showImage() -> UIButton {
        let viewHeight = UIScreen.main.bounds.size.height
        let viewWidth = UIScreen.main.bounds.size.width

        let rectSideSize = CGFloat.random(in: 70...150)
            
        let randomNumberOne = CGFloat.random(in: 10...viewWidth-100)
        let randomNumberTwo = CGFloat.random(in: 10...viewHeight-100)
            
        let button = UIButton(frame: CGRect(x: randomNumberOne , y: randomNumberTwo , width: rectSideSize, height: rectSideSize))
        button.setImage(UIImage(named: "smile"), for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.addTarget(self, action: #selector(animationForImage), for: .touchDown)
            
        return button
    }
    
    func addView() {
        let newButton = showImage()
        start = NSDate()
        self.view.addSubview(newButton)
        buttonImage = newButton
        counter += 1
    }
    
    @objc func animationForImage() {
        guard let frmPlay = self.buttonImage?.frame else { return }
        UIView.animate(withDuration: 0,
                       animations: {
            self.buttonImage?.frame = CGRect(
                x: frmPlay.origin.x,
                y: frmPlay.origin.y,
                width: frmPlay.size.width + 10,
                height: frmPlay.size.height + 10)

            }, completion: { finished in
        })
    }
    
    @objc func buttonAction(sender: UIButton!) {
        end = NSDate()
        result += (end.timeIntervalSince(start as Date))
        buttonImage?.removeFromSuperview()
        if counter < 10 {
            addView()
        } else {
            transitionOnNextVC()
        }
    }

    func transitionOnNextVC(){
        let resultVC = ResultViewController(nibName: "ResultViewController", bundle: nil)
        if result <= 7 {
            resultVC.isWinner = true
        } else {
            resultVC.isWinner = false
        }
        navigationController?.pushViewController(resultVC, animated: true)
    }

    @IBAction func startGameAction(_ sender: Any) {
        startButton.alpha = 0
        let alertShown = UserDefaults.standard.bool(forKey: "ShownAlert")
        if !alertShown {
            let alert = AlertManager().showAlert(){data in
                self.start = data
            }
            present(alert, animated: true, completion: nil);

               UserDefaults.standard.set(true, forKey: "ShownAlert")
        }
        addView()
    }
    
}

