
import UIKit

class ResultViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    var isWinner: Bool = false
    var linkPrize: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadResults()

        switch self.isWinner {
        case true:
            self.resultLabel.text = "YOU WON!"
        case false:
            self.resultLabel.text = "YOU LOSE"
        }
    }
    
    func loadResults(){
         NetworkManager.shared.loadResult(isWinner: isWinner){data, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
             DispatchQueue.main.async {
                 self.linkPrize = data
             }
        }
    }

    @IBAction func showPrizeAction(_ sender: Any) {
        guard let link = linkPrize, let url = URL(string: link) else{return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func playAgainAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
