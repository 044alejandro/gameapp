
import Foundation

struct Results: Codable {
    let winner: String
    let loser: String
}
