
import UIKit

class AlertManager {
    
    func showAlert(completionHandler: @escaping(NSDate) -> Void) -> UIAlertController{
        let alertVC = UIAlertController(title: "Правила игры", message: """
1. Смайл появляется в случайном месте экрана
2. Успеть в течении 7 секунд поймать всех смайликов
3. Для прохождения игры требуется поймать 10 смайликов
""", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) {_ in
            let start = NSDate()
            completionHandler(start)
        }
        alertVC.addAction(cancelAction)
        return alertVC
    }
}
